# First create the view

CREATE VIEW monitoring_synthesis AS
(
SELECT WR.Url                                  'URL',
       a.Dt_Creation                           'Date',
       a.Status                                'Statut',
       COALESCE(WRS.Nb_Failed_Occurrences, "") 'Erreurs',
       COALESCE(WRS.Nb_Na, "")                 'Tests non-applicables',
       COALESCE(WRS.Nb_Passed, "")             'Tests conformes',
       COALESCE(WRS.Nb_Failed, "")             'Tests non-conformes',
       COALESCE(WRS.Nb_Nmi, "")                'Tests pré-qualifiés',
       COALESCE(WRS.Nb_Not_Tested, "")         'Tests non-testés',
       (CASE
            WHEN WRS.Raw_Mark IS NULL THEN ""
            WHEN WRS.Raw_Mark > 99 THEN "A"
            WHEN WRS.Raw_Mark > 90 THEN "B"
            WHEN WRS.Raw_Mark > 85 THEN "C"
            WHEN WRS.Raw_Mark > 75 THEN "D"
            WHEN WRS.Raw_Mark > 60 THEN "E"
            ELSE "F"
           END) as                             'Note Asqatasun'
FROM AUDIT as a
         INNER JOIN WEB_RESOURCE WR on a.Id_Audit = WR.Id_Audit
         LEFT JOIN WEB_RESOURCE_STATISTICS WRS on a.Id_Audit = WRS.Id_Audit
ORDER BY WR.Url
    );

# Then export data into CSV file
SELECT *
INTO OUTFILE '/tmp/Observatoire_Accessibilite_2023-12-29.csv'
    CHARACTER
SET utf8mb4
    FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
    LINES TERMINATED BY '\n'
FROM (SELECT 'URL',
             'Date',
             'Statut',
             'Erreurs',
             'Tests non-applicables',
             'Tests conformes',
             'Tests non-conformes',
             'Tests pré-qualifiés',
             'Tests non-testés',
             'Note Asqatasun'
      UNION ALL
      SELECT *
      from monitoring_synthesis)
         as mycsv
;
