# Description du jeu de données

## `URL`

URL auditée. Les URLs sont construites par le
dépôt [Noms de domaine du secteur public](https://gitlab.adullact.net/dinum/noms-de-domaine-organismes-secteur-public/).
Toutes les contraintes et restrictions y sont documentées.

Il se peut que certains sites apparaissent plusieurs fois. Pour le domaine `exemple.fr`, c'est le cas quand :

* il n'existe pas de redirection amenant `http//exemple.fr` vers `https//exemple.fr` (HTTPS)
* ou il n'existe pas de redirection amenant `https//www.exemple.fr` vers `https//exemple.fr` (avec / sans www)

## `Date`

La date de l'audit.

## `Statut`

La valeur `COMPLETED` indique un audit terminé. Toutes les autres valeurs décrivent un audit qui n'a pas abouti, auquel
cas aucune valeur n'est présentée. Ceci peut être le cas si le site ne répond pas (ou pas assez vite) lors de l'audit.

## `Erreurs`

Du point de vue du RGAA, toutes les erreurs du même type sont rangées dans un seul et même test, qualifié de
non-conforme. L'observatoire se concentre sur la quantification des erreurs. Le **nombre d'erreurs** est défini comme
étant la somme des erreurs de tous les tests non-conformes.

## `Tests non-applicables`

Test non-applicable tel que décrit par le RGAA

## `Tests conformes`

Test conforme tel que décrit par le RGAA

## `Tests non-conformes`

Test non-conforme tel que décrit par le RGAA

## `Tests pré-qualifiés`

Test pour lequel Asqatasun a pu récupérer des informations utiles à l'auditeur (gain de temps), mais sans pour autant
pouvoir déterminer un résultat avec certitude.

C'est un résultat de type *cannot tell* au sens de WCAG.

## `Tests non-testés`

Test qui n'est pas traité automatiquement.

## `Note Asqatasun`

Note sous forme de lettre telle que définie dans la 
[documentation Asqatasun : *Asqatasun meter*](https://doc.asqatasun.org/v5/en/User/Asqatasun-meter/)
