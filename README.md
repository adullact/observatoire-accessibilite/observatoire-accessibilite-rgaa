# Observatoire accessibilité RGAA

L'observatoire de l'accessibilité a pour but d'aider le **pilotage** de la mise en accessibilité des sites web publics.
Il s'adresse autant à l'**élu** qu'au **développeur**.

L'analyse se fait au regard du **RGAA** (Référentiel Générale d'Amélioration de l'Accessibilité). Ce dernier propose des
indicateurs précis, mais difficiles à appréhender (tests conformes, non-conformes, non-applicables).

L'observatoire renseigne tous les indicateurs du RGAA, avec pour objectif de passer au crible tous les sites web
publics, et de vérifier leur conformité.

## Un nouvel indicateur

Dans le but d'accompagner les acteurs publics, l'observatoire introduit un nouvel indicateur : le **nombre d'erreurs**.

Du point de vue du RGAA, toutes les erreurs du même type sont rangées dans un seul et même test, qualifié de
non-conforme. L'observatoire se concentre sur la quantification des erreurs. Le **nombre d'erreurs** est défini comme
étant la somme des erreurs de tous les tests non-conformes.

## Volumétrie et périmètre

L'observatoire réutilise le jeu de
données [Noms de domaine du secteur public](https://gitlab.adullact.net/dinum/noms-de-domaine-organismes-secteur-public/)
géré par la DINUM (Direction Interministérielle du Numérique).

Ce référentiel contient **100 000** entrées, ce qui produit 37 000 sites web répondant à une requête HTTP ou HTTPS (
été 2024).

La mesure porte sur la page d'accueil du site. À l'avenir, elle sera opérée sur l'ensemble du site.

## Technique et limitations

L'observatoire ne traite que la partie automatisable du RGAA et ne saurait remplacer un audit complet fait par un
spécialiste.

L'audit automatisé est assuré par le [logiciel libre Asqatasun](https://gitlab.com/asqatasun/Asqatasun). Ce dernier
couvre [45% du RGAA](https://doc.asqatasun.org/v5/en/Business-rules/RGAA-v4/) (117 tests couverts sur les 257 du RGAA)

En décembre 2024, ce sont Asqatasun v6.0.0-rc.1 et le RGAA 4.1.2 qui sont utilisés.
